package com.client.visitingcard

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView

class ReadQrCode : Fragment(), ZXingScannerView.ResultHandler {
    var mActivity: Activity? = null
    var qrCodeScanner: ZXingScannerView? = null
    var txt_url: TextView? = null

    companion object {
        var readfragment: ReadQrCode? = null
        fun getInstance(): ReadQrCode {
            readfragment = ReadQrCode()

            return readfragment!!
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mActivity = activity
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as Activity
    }


    override fun handleResult(p0: Result?) {
        if (p0 != null) {
            Toast.makeText(context, p0.text, Toast.LENGTH_LONG).show()
            txt_url!!.setText(p0.text)
//            startActivity(Intent(context, RegisterActivity::class.java))
//            finish()
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view: View = inflater.inflate(R.layout.activity_read_scanner, container, false)

        qrCodeScanner = view.findViewById(R.id.qrCodeScanner) as (ZXingScannerView)
        txt_url = view.findViewById(R.id.txt_url) as (TextView)

        setScannerProperties()
        return view
    }

    private fun setScannerProperties() {
        qrCodeScanner!!.setFormats(listOf(BarcodeFormat.QR_CODE))
        qrCodeScanner!!.setAutoFocus(true)
        qrCodeScanner!!.setLaserColor(R.color.colorAccent)
        qrCodeScanner!!.setMaskColor(R.color.colorAccent)
//        if (Build.MANUFACTURER.equals(HUAWEI, ignoreCase = true))
        qrCodeScanner!!.setAspectTolerance(0.5f)
    }

    override fun onResume() {
        super.onResume()

        qrCodeScanner!!.startCamera()
        qrCodeScanner!!.setResultHandler(this@ReadQrCode)
    }

    override fun onPause() {
        super.onPause()
        qrCodeScanner!!.stopCamera()
    }
}