package com.client.visitingcard.Interface

import org.json.JSONObject

interface RetrofitResponseInterface {
    fun <T> onSuccess(apiType:String, response:T)
    fun onFailure(apiType:String)
    fun onServerFailure(obj : JSONObject)
}