package com.client.visitingcard.Interface

import com.client.visitingcard.Model.CardModel
import com.client.visitingcard.Model.LoginModel
import com.client.visitingcard.Model.LogoutModel
import com.client.visitingcard.Model.RegisterModel
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface RetrofitInterfaceClass {


    @POST("/api/login")
    @FormUrlEncoded
    fun getLoginService(@Header("Accept") accept:String,@Header("Content-Type") content:String, @Field("email") username: String, @Field("password") password: String): Call<LoginModel>

//    @POST("/api/register")
//    @FormUrlEncoded
//    fun getRegisterService(
//        @HeaderMap map: Map<String, String>, @Field("username") username: String, @Field("email") userEmail: String, @Field(
//            "password"
//        ) password: String, @Field("c_password") confirm_password: String, @Field("phone") phone: String
//    ): Call<RegisterModel>

//    @GET("/api/logout")
//    @FormUrlEncoded
//    fun getLogout(@HeaderMap map: Map<String, String>): Call<LogoutModel>

    @Multipart
    @POST("/api/setbudgetcard")
    fun getCardUrl(
        @Header("Accept") accept:String,
        @Header("Authorization") authorization:String,
        @Part("title") title: RequestBody,
        @Part("designation") designation: RequestBody,
        @Part("call_me") phoneno: RequestBody,
        @Part("sms_me") altphone_no: RequestBody,
        @Part("font_color") color1: RequestBody,
        @Part("background_color") color2: RequestBody,
        @Part("sms_background_color") color3: RequestBody,
        @Part("call_background_color") color4: RequestBody,
        @Part file: MultipartBody.Part,
        @Part file1: MultipartBody.Part
    ): Call<CardModel>



}