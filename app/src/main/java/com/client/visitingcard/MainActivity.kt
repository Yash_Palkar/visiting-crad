package com.client.visitingcard

import android.app.Activity
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.View
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.ViewGroup


class MainActivity : Fragment() {

    var mActivity: Activity? = null

    companion object {
        var mainactivity: MainActivity? = null
        fun getInstance(): MainActivity {
            mainactivity = MainActivity()

            return mainactivity!!
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mActivity = activity
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as Activity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view: View = inflater.inflate(R.layout.activity_main, container, false)
        var getData = dataSource()
        var viewPager = view.findViewById(R.id.viewpager) as (ViewPager)

        var mCustomPagerAdapter = CustomPagerAdapter(context!!, getData)
        viewPager.setAdapter(mCustomPagerAdapter)
        viewPager.setClipToPadding(false)
        viewPager.setPadding(80, 0, 80, 0)
        viewPager.setPageMargin(20)

        return view
    }

    private fun dataSource(): List<CardsModel> {
        var data = ArrayList<CardsModel>()
        data.add(CardsModel(R.color.colorPrimary, "Budget Card"))
        data.add(CardsModel(R.color.colorPrimaryDark, "Premium Card"))
        data.add(CardsModel(R.color.colorAccent, "Invitation Card"))
        data.add(CardsModel(R.color.colorPrimary, "Portfolio Card"))
        return data
    }

}
