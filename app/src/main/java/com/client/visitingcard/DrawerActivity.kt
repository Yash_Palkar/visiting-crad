package com.client.visitingcard

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.client.visitingcard.ApiManager.ApiSingletonClass
import com.client.visitingcard.HelperClass.CheckInternetConnection
import com.client.visitingcard.HelperClass.SharedPreferenceClass
import com.client.visitingcard.Interface.RetrofitResponseInterface
import com.client.visitingcard.Model.LoginModel
import com.client.visitingcard.Model.LogoutModel
import kotlinx.android.synthetic.main.activity_drawer.*
import org.json.JSONObject

class DrawerActivity : AppCompatActivity() {
    var dLayout: DrawerLayout? = null
    var redirect = ""
    var card_url = ""
    var title = ""
    var drawerToggle: ActionBarDrawerToggle? = null
    private var mToolBarNavigationListenerIsRegistered = false
    var level = ""
    var alert: AlertDialog? = null
    var level_no = 0
    private lateinit var progressBar: ProgressBar
    lateinit var apiSingleton: ApiSingletonClass
    var prefconfig: SharedPreferenceClass? = null
    var drawer_title: TextView? = null
    private var checkInternet = CheckInternetConnection()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drawer)
        apiSingleton = ApiSingletonClass()
        checkInternet = CheckInternetConnection()
        // Configure action bar
        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        supportActionBar!!.setDisplayShowTitleEnabled(false)
//        actionBar?.title = "Hello Toolbar"
        drawer_title = findViewById(R.id.drawer_title)
        drawer_title!!.setOnClickListener { }
        prefconfig = SharedPreferenceClass(applicationContext)
        // Initialize the action bar drawer toggle instance
        drawerToggle = object : ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbar,
            R.string.drawer_open,
            R.string.drawer_close
        ) {
            override fun onDrawerClosed(view: View) {
                super.onDrawerClosed(view)
                //toast("Drawer closed")
            }

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                //toast("Drawer opened")
            }
        }

        // Configure the drawer layout to add listener and show icon on toolbar
        drawerToggle!!.isDrawerIndicatorEnabled = true
        drawer_layout.addDrawerListener(drawerToggle!!)
        drawerToggle!!.syncState()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            drawerToggle!!.getDrawerArrowDrawable().setColor(getColor(R.color.white))
        } else {
            drawerToggle!!.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white))
        }
        intent = intent
        if (intent.extras != null) if (intent.extras!!.getString("redirect") != null) redirect =
            intent.extras!!.getString("redirect")!!
        if (intent.extras != null) if (intent.extras!!.getString("card_url") != null) card_url =
            intent.extras!!.getString("card_url")!!

        if (redirect.equals("")) {

            val firstFragment = MainActivity()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()
            transaction.add(R.id.content_frame, firstFragment)
//            transaction.addToBackStack("main_fragment")
            transaction.commit()

        } else {
            fragmenttoreplace(redirect)
        }


        // Set navigation view navigation item selected listener
        navigation_view.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_tables -> {
                    clearBackStack()
                    enableViews(true, drawerToggle!!)
                    if (checkInternet.isNetworkAvailable(applicationContext)) {
                        apiSingleton.showDialog(progressBar)
                        postlogout()
                    } else {
                        Toast.makeText(applicationContext, "Check Network Connectivity", Toast.LENGTH_SHORT).show()
                    }
//                    val firstFragment = MathsTableFragment()
//                    val manager = supportFragmentManager
//                    val transaction = manager.beginTransaction()
//                    transaction.add(R.id.content_frame, firstFragment)
//                    transaction.addToBackStack("action_tables")
//                    transaction.commit()
                }
//                R.id.action_formula -> {
//                    clearBackStack()
//                    enableViews(true, drawerToggle!!)
//                    val bundle = Bundle()
//                    bundle.putString("from_where", "1")
//                    val firstFragment = ShowDataFormulaTrick()
//                    firstFragment.arguments = bundle
//                    val manager = supportFragmentManager
//                    val transaction = manager.beginTransaction()
//                    transaction.add(R.id.content_frame, firstFragment)
//                    transaction.addToBackStack("action_formula")
//                    transaction.commit()
//                }
//                R.id.action_math -> {
//                    clearBackStack()
//                    enableViews(true, drawerToggle!!)
//                    val bundle = Bundle()
//                    bundle.putString("from_where", "2")
//                    val firstFragment = ShowDataFormulaTrick()
//                    firstFragment.arguments = bundle
//                    val manager = supportFragmentManager
//                    val transaction = manager.beginTransaction()
//                    transaction.add(R.id.content_frame, firstFragment)
//                    transaction.addToBackStack("action_math")
//                    transaction.commit()
//                }
//                R.id.action_achievement -> {
//                    // Multiline action
//                    clearBackStack()
//                    enableViews(true, drawerToggle!!)
//                    val bundle = Bundle()
//                    bundle.putString("from_where", "2")
//                    val firstFragment = AchievementFragment()
//                    firstFragment.arguments = bundle
//                    val manager = supportFragmentManager
//                    val transaction = manager.beginTransaction()
//                    transaction.add(R.id.content_frame, firstFragment)
//                    transaction.addToBackStack("action_achievement")
//                    transaction.commit()
//                }
//                R.id.action_reset -> {
//                    prefconfig!!.clearpreference()
//                    val firstFragment = HomeFragment()
//                    val manager = supportFragmentManager
//                    val transaction = manager.beginTransaction()
//                    transaction.add(R.id.content_frame, firstFragment)
//                    transaction.addToBackStack("home_fragment")
//                    transaction.commit()
//                }
//                R.id.privacy_policy -> {
//                    prefconfig!!.clearpreference()
//                    val firstFragment = PrivacyFragment()
//                    val manager = supportFragmentManager
//                    val transaction = manager.beginTransaction()
//                    transaction.add(R.id.content_frame, firstFragment)
//                    transaction.addToBackStack("action_policy")
//                    transaction.commit()
//                }

            }
            // Close the drawer
            drawer_layout.closeDrawer(GravityCompat.START)
            true
        }
    }

    private fun postlogout() {
//        val apiInterface = apiSingleton.getInstance().createService()
//
//        val map: HashMap<String, String> = HashMap()
//        map["Accept"] = "application/json"
//        map["Content-Type"] = "application/x-www-form-urlencoded"
//
//        val call = apiInterface.getLogout(map)
//        apiSingleton.getInstance().getData(call, object : RetrofitResponseInterface {
//            override fun onServerFailure(obj: JSONObject) {
//                Log.d("TAG", "RESPONSE FAILURE = " + obj.getString("message"))
//                responseFailure(obj.getString("message"))
//            }
//
//            override fun <T> onSuccess(apiType: String, response: T) {
//                apiSingleton.dismissDialog(progressBar)
//                val userData = response as LogoutModel
//                val hashMap: HashMap<String, String> = HashMap()
////                hashMap["token"] = userData.success!!.token.toString()
////                prefconfig!!.setUserData(hashMap)
////                sharedPref.saveUserData(getString(R.string.user), Boolean::class.java, true)
//                startActivity(Intent(applicationContext, LoginActivity::class.java))
//                finish()
//            }
//
//            override fun onFailure(apiType: String) {
//                responseFailure("")
//            }
//        })
    }

    fun responseFailure(message: String) {
        apiSingleton.dismissDialog(progressBar)

    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return if (drawerToggle!!.onOptionsItemSelected(item)) {
            true
        } else super.onOptionsItemSelected(item)


    }

    private fun clearBackStack() {
        var fragmentManager = supportFragmentManager
        for (i in 0 until supportFragmentManager.backStackEntryCount)
            fragmentManager.popBackStack()
    }

    private fun fragmenttoreplace(redirect: String) {
        when (redirect) {
            "0" -> {
                enableViews(true, drawerToggle!!)
                val bundle = Bundle()
//                bundle.putString("title", title)
//                bundle.putInt("getLevelNumber", getLevelNumber(title))
                val firstFragment = DashboardActivity()
                firstFragment.arguments = bundle
                val manager = supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.add(R.id.content_frame, firstFragment)
//                transaction.addToBackStack("dash_fragment")
                transaction.commit()
            }
            "1" -> {
                enableViews(true, drawerToggle!!)
                val bundle = Bundle()
//                bundle.putString("title", title)
//                bundle.putString("level", level)
//                bundle.putInt("level_no", getLevelNumber(title))
                val firstFragment = DetailActiivty()
                firstFragment.arguments = bundle
                val manager = supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.add(R.id.content_frame, firstFragment)
//                transaction.addToBackStack("detail_fragment")
                transaction.commit()
            }
            "3" -> {
                enableViews(true, drawerToggle!!)
                val bundle = Bundle()
//                bundle.putString("title", title)
                val firstFragment = ReadQrCode()
                firstFragment.arguments = bundle
                val manager = supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.add(R.id.content_frame, firstFragment)
//                transaction.addToBackStack("readqr_fragment")
                transaction.commit()
            }
            "4" -> {
                enableViews(true, drawerToggle!!)
                val bundle = Bundle()
//                bundle.putString("title", title)
//                bundle.putInt("level_no", level_no)
              var i= Intent(this,WebViewActivity::class.java)
                i.putExtra("card_url",card_url)
                startActivity(i)
            }
            else -> {
                val firstFragment = MainActivity()
                val manager = supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.add(R.id.content_frame, firstFragment)
//                transaction.addToBackStack("main_fragment")
                transaction.commit()
            }
        }
    }

    private fun Context.toast(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }

    fun enableViews(enable: Boolean, drawerToggle: ActionBarDrawerToggle) {

        if (enable) {
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            drawerToggle!!.setDrawerIndicatorEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            if (!mToolBarNavigationListenerIsRegistered) {
                drawerToggle!!.setToolbarNavigationClickListener(View.OnClickListener {
                                        val f = this.supportFragmentManager.findFragmentById(R.id.content_frame)
//                    if (f is ) {
//                        (f as QuestionViewFragment).setBackDialog()
//                    } else if (f is TrickyFragment) {
//                        (f as TrickyFragment).setBackDialog()
//                    } else if(f is HighScore)
//                    {
//                        (f as HighScore).btn_dismiss!!.performClick()
//                    } else {
//                        onBackPressed()
//                    }
                })

                mToolBarNavigationListenerIsRegistered = true
            }

        } else {
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
            drawerToggle!!.setDrawerIndicatorEnabled(true)
            drawerToggle!!.setToolbarNavigationClickListener(null)
            mToolBarNavigationListenerIsRegistered = false
        }

    }


    fun callExitDialog() {
        val builder = AlertDialog.Builder(this@DrawerActivity)
        builder.setTitle("Exit")
        builder.setCancelable(false)
        builder.setMessage("Are you sure you want to exit the application?")
        builder.setPositiveButton("Yes") { dialog, which ->
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
        }
        builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
        alert = builder.create()
        alert!!.show()
    }

//    override fun onBackPressed() {
//        val fragmentManager = this@DrawerActivity.supportFragmentManager as FragmentManager
//        val f = fragmentManager.findFragmentById(R.id.content_frame)
//        val count_new = fragmentManager.backStackEntryCount
//        if (count_new > 0) {
//            var backStackName =
//                fragmentManager.getBackStackEntryAt(fragmentManager.backStackEntryCount - 1).name
//            if (backStackName != null) {
//                if ((backStackName == "action_tables" || backStackName == "levelview_fragment" || backStackName == "action_formula" || backStackName == "action_math" || backStackName == "action_achievement" )) {
//                    enableViews(false, drawerToggle!!)
//                    val firstFragment = HomeFragment()
//                    val manager = supportFragmentManager
//                    val transaction = manager.beginTransaction()
//                    transaction.replace(R.id.content_frame, firstFragment)
//                    transaction.addToBackStack("home_fragment")
//                    transaction.commit()
//                } else if (backStackName == "questionview_fragment") {
//                    (f as QuestionViewFragment).setBackDialog()
////                    fragmentManager.popBackStack()
//                }else if (backStackName == "highscore") {
//                    (f as HighScore).btn_dismiss!!.performClick()
////                    fragmentManager.popBackStack()
//                }else if(backStackName == "tricky_fragment")
//                {
//                    (f as TrickyFragment).setBackDialog()
//                }
//                else if (backStackName == "home_fragment") {
//                    callExitDialog()
//                }
//            }
//        }
//    }
}