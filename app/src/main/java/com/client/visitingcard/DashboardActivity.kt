package com.client.visitingcard

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button


class DashboardActivity : Fragment() {
    var mActivity:Activity?=null
    var recyclerView: RecyclerView? = null
    var mAdapter: DashboardAdapter? = null
    var btn_createcard_redirect: Button? = null
    var arraylist: ArrayList<DashModel>? = null
    companion object {
        var homefragment: DashboardActivity? = null
        fun getInstance(): DashboardActivity {
            homefragment = DashboardActivity()

            return homefragment!!
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mActivity = activity
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as Activity
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view: View = inflater.inflate(R.layout.activity_dashboard, container, false)

        btn_createcard_redirect=view.findViewById(R.id.btn_createcard_redirect) as Button
        btn_createcard_redirect!!.setOnClickListener {
//            context!!.startActivity(Intent(context,DetailActiivty::class.java))

        }
        arraylist = ArrayList<DashModel>()
        arraylist!!.add(DashModel("Created Cards", "10,0000"))
        arraylist!!.add(DashModel("Created Cards", "20,0000"))
        arraylist!!.add(DashModel("Created Cards", "50,0000"))
        arraylist!!.add(DashModel("Created Cards", "10,0000"))
        arraylist!!.add(DashModel("Created Cards", "20,0000"))
        arraylist!!.add(DashModel("Created Cards", "50,0000"))
        arraylist!!.add(DashModel("Created Cards", "10,0000"))
        arraylist!!.add(DashModel("Created Cards", "20,0000"))
        arraylist!!.add(DashModel("Created Cards", "50,0000"))
        recyclerView = view.findViewById(R.id.recycler_view) as RecyclerView
        recyclerView!!.setLayoutManager(GridLayoutManager(context,2))

        if (mAdapter == null) {
            mAdapter = DashboardAdapter(context!!,arraylist!!)
            recyclerView!!.setAdapter(mAdapter)
        }
        return view
    }
}
