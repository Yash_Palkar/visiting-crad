package com.client.visitingcard

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.webkit.WebView
import android.webkit.WebViewClient
import android.content.Intent
import android.net.Uri
import android.webkit.URLUtil
import android.webkit.URLUtil.isNetworkUrl
import android.content.pm.PackageManager





class WebViewActivity : AppCompatActivity() {

    private lateinit var webView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)
        webView = findViewById(R.id.webview)
        val ss:String = intent.getStringExtra("card_url")
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                if (url!!.startsWith("http:") || url!!.startsWith("https:")) {
                    return false
                }

                // Otherwise allow the OS to handle things like tel, mailto, etc.
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(intent)
                return true
            }
        }
        webView.loadUrl(ss)
        webView.getSettings().setJavaScriptEnabled(true)
//        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
    }

}