package com.client.visitingcard.HelperClass

import android.content.Context
import android.content.SharedPreferences
import com.client.visitingcard.R
import java.util.HashMap

class SharedPreferenceClass(var context: Context) {
    var sharedPreferences: SharedPreferences
    lateinit var edit: SharedPreferences.Editor

    init {
        sharedPreferences = context.getSharedPreferences(
            context.resources.getString(R.string.first_time_install),
            Context.MODE_PRIVATE
        )
    }

    fun setUserData(users: HashMap<String, String>) {
        edit = sharedPreferences.edit()
        edit.putString(context.resources.getString(R.string.token), users["token"])
        edit.apply()

    }

    fun getKey(getKey: String): String {
        return sharedPreferences.getString(getKey, "")
    }

    fun <T> saveUserData(key: String, dataType: Class<T>, data: T) {
        edit = sharedPreferences.edit()
        if (dataType == String::class.java) {
            edit.putString(key, data.toString())
        } else if (dataType == Integer::class.java) {
            edit.putInt(key, Integer.parseInt(data.toString()))
        } else if (dataType == Boolean::class.java) {
            edit.putBoolean(key, data as Boolean)
        }
        edit.apply()
    }

    fun <T> getUserData(key: String, dataType: Class<T>): T {
        var data: T? = null
        when (dataType) {
            String::class.java -> data = sharedPreferences.getString(key, "") as T
            Integer::class.java -> data = sharedPreferences.getInt(key, 0) as T
            Boolean::class.java -> data = sharedPreferences.getBoolean(key, false) as T
        }
        return data!!
    }

}