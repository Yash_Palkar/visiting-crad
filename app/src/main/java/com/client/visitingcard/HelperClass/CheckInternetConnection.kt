package com.client.visitingcard.HelperClass

import android.content.Context
import android.net.ConnectivityManager

class CheckInternetConnection {
    fun isNetworkAvailable(activity: Context): Boolean {
        val connectivityManager = activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

}