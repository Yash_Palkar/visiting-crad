package com.client.visitingcard.Model

import com.google.gson.annotations.SerializedName

class RegisterModel {
    @SerializedName("success")
    var success: TokenDetail? = null

    class TokenDetail {
        @SerializedName("token")
        var token: String? = null



    }
    @SerializedName("userdata")
    var userDetails: UserDetail? = null
    class UserDetail{

        @SerializedName("id")
        var id: String? = null

        @SerializedName("username")
        var username: String? = null

        @SerializedName("email")
        var email: String? = null

        @SerializedName("phone")
        var phone: String? = null


        @SerializedName("created_at")
        var created_at: String? = null

        @SerializedName("updated_at")
        var updated_at: String? = null

    }
}