package com.client.visitingcard.Model

import com.google.gson.annotations.SerializedName

class LoginModel {

    @SerializedName("success")
    var success: TokenDetail? = null

    class TokenDetail {
        @SerializedName("token")
        var token: String? = null

    }
    @SerializedName("userdata")
    var userDetails: UserDetail? = null
    class UserDetail{

        @SerializedName("id")
        var id: String? = null

        @SerializedName("first_name")
        var first_name: String? = null

        @SerializedName("last_name")
        var last_name: String? = null

        @SerializedName("username")
        var username: String? = null

        @SerializedName("email")
        var email: String? = null

        @SerializedName("address")
        var address: String? = null

        @SerializedName("phone")
        var phone: String? = null

        @SerializedName("country")
        var country: String? = null

        @SerializedName("date_of_birth")
        var date_of_birth: String? = null

        @SerializedName("role")
        var role: String? = null

        @SerializedName("status")
        var status: String? = null

        @SerializedName("gender")
        var gender: String? = null

        @SerializedName("image")
        var image: String? = null

        @SerializedName("google")
        var google: String? = null

        @SerializedName("facebook")
        var facebook: String? = null

        @SerializedName("twitter")
        var twitter: String? = null

        @SerializedName("linkedin")
        var linkedin: String? = null

        @SerializedName("skype")
        var skype: String? = null

        @SerializedName("dribbble")
        var dribbble: String? = null

        @SerializedName("created_at")
        var created_at: String? = null

        @SerializedName("updated_at")
        var updated_at: String? = null

    }
}