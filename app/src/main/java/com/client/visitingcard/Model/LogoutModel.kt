package com.client.visitingcard.Model

import com.google.gson.annotations.SerializedName

class LogoutModel {
    @SerializedName("Success")
    var success: String? = null
}