package com.client.visitingcard

import android.content.Context
import android.content.Intent
import android.support.constraint.ConstraintLayout
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView

import android.support.v7.widget.CardView
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.TextView


class CustomPagerAdapter(private val context: Context, private val dataObjectList: List<CardsModel>) : PagerAdapter() {
    private val layoutInflater: LayoutInflater

    init {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getCount(): Int {
        return dataObjectList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as View
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = this.layoutInflater.inflate(R.layout.activity_item, container, false)
        val displayImage = view.findViewById(R.id.card_color) as CardView
        val txt_card = view.findViewById(R.id.txt_card) as TextView
        displayImage.setCardBackgroundColor(context.resources.getColor(this.dataObjectList[position].imageId))
        displayImage.setOnClickListener {

            var i = Intent(context, DrawerActivity::class.java)
            i.putExtra("redirect","0")
            context.startActivity(i)

        }
        txt_card.setText(this.dataObjectList[position].imageName)
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}