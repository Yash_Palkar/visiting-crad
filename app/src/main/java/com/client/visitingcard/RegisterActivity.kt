package com.client.visitingcard

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.client.visitingcard.ApiManager.ApiSingletonClass
import com.client.visitingcard.HelperClass.CheckInternetConnection
import com.client.visitingcard.HelperClass.SharedPreferenceClass
import com.client.visitingcard.Interface.RetrofitResponseInterface
import com.client.visitingcard.Model.LoginModel
import com.client.visitingcard.Model.RegisterModel
import org.json.JSONObject

class RegisterActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var edit_rusername: EditText
    lateinit var edit_email: EditText
    lateinit var edit_rpassword: EditText
    lateinit var edit_confirm_password: EditText
    lateinit var edit_phone: EditText
    private lateinit var progressBar: ProgressBar

    lateinit var sharedPref: SharedPreferenceClass
    lateinit var apiSingleton: ApiSingletonClass
    private var checkInternet = CheckInternetConnection()
    var btn_register: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        btn_register = findViewById(R.id.btn_register) as Button
        sharedPref = SharedPreferenceClass(applicationContext)
        apiSingleton = ApiSingletonClass()
        checkInternet = CheckInternetConnection()

        edit_rusername = findViewById(R.id.edit_rusername)
        edit_email = findViewById(R.id.edit_email)
        edit_rpassword = findViewById(R.id.edit_rpassword)
        edit_confirm_password = findViewById(R.id.edit_confirm_password)
        edit_phone = findViewById(R.id.edit_phone)
        btn_register = findViewById(R.id.btn_register)


        btn_register!!.setOnClickListener(this)
    }

    private fun userRegister() {
        val userUsername: String = edit_rusername.text.toString()
        val userEmail: String = edit_email.text.toString()
        val userPassword: String = edit_rpassword.text.toString()
        val userconfirmPassword: String = edit_confirm_password.text.toString()
        val phone: String = edit_phone.text.toString()
        if (userUsername == "" || userEmail == "" || userPassword == "" || userconfirmPassword == ""|| phone == "") {
            Toast.makeText(applicationContext, "Enter Values First!", Toast.LENGTH_SHORT).show()
        } else {
            if (checkInternet.isNetworkAvailable(applicationContext)) {
//                apiSingleton.showDialog(progressBar)
                postRegister(userUsername, userEmail, userPassword, userconfirmPassword,phone)
            } else {
                Toast.makeText(applicationContext, "Check Network Connectivity", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun postRegister(userUsername: String, userEmail: String, password: String, confirm_password: String, phone: String) {
//               val apiInterface = apiSingleton.getInstance().createService()
//
//        val map: HashMap<String, String> = HashMap()
//        map["Accept"] = "application/json"
//        map["Content-Type"] = "application/x-www-form-urlencoded"
//
//        val call = apiInterface.getRegisterService(map,userUsername, userEmail, password, confirm_password,phone)
//        apiSingleton.getInstance().getData(call, object : RetrofitResponseInterface {
//            override fun onServerFailure(obj: JSONObject) {
//                Log.d("TAG", "RESPONSE FAILURE = " + obj.getString("message"))
//                responseFailure(obj.getString("message"))
//            }
//
//            override fun <T> onSuccess(apiType: String, response: T) {
////                apiSingleton.dismissDialog(progressBar)
//                val userData = response as RegisterModel
//                val hashMap: HashMap<String, String> = HashMap()
//                hashMap["token"] = userData.success!!.token.toString()
//                sharedPref.setUserData(hashMap)
////                sharedPref.saveUserData(getString(R.string.user), Boolean::class.java, true)
//                startActivity(Intent(applicationContext, LoginActivity::class.java))
//                finish()
//            }
//
//            override fun onFailure(apiType: String) {
//                responseFailure("")
//            }
//        })
    }

    fun responseFailure(message: String) {
        apiSingleton.dismissDialog(progressBar)

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btn_register -> {
//                userRegister()
                startActivity(Intent(applicationContext, LoginActivity::class.java))
                finish()
            }
        }
    }
}
