package com.client.visitingcard

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView

class DashboardAdapter(var context:Context,private val list: List<DashModel>)
    : RecyclerView.Adapter<DashboardAdapter.MovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return MovieViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie: DashModel = list[position]
        holder.bind(movie)

    }

    override fun getItemCount(): Int = list.size
   inner class MovieViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.activity_dash_item, parent, false)) {
        private var mTitleView: TextView? = null
        private var mYearView: TextView? = null


        init {
            mTitleView = itemView.findViewById(R.id.txt_label_total)
            mYearView = itemView.findViewById(R.id.txt_total)
        }

        fun bind(movie: DashModel) {
            mTitleView?.text = movie.imageId
            mYearView?.text = movie.imageName

            mTitleView!!.setOnClickListener {
                var intent = Intent(context, DrawerActivity::class.java)
                intent.putExtra("redirect","1")
                context.startActivity(intent)
            }
        }

    }
}