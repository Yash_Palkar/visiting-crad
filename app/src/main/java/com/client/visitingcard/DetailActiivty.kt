package com.client.visitingcard

import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.ContentResolver
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.view.View
import android.view.animation.AnimationUtils
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.Window
import android.webkit.MimeTypeMap
import android.widget.*
import com.client.visitingcard.ApiManager.ApiSingletonClass
import com.client.visitingcard.Interface.RetrofitResponseInterface
import com.client.visitingcard.Model.CardModel
import com.client.visitingcard.Model.RegisterModel
import okhttp3.Headers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


class DetailActiivty : Fragment(), ProgressRequestBody.UploadCallbacks {
    override fun onFinish() {

    }

    override fun onProgressUpdate(percentage: Int) {

    }

    override fun onError() {

    }

    private val RESULT_LOGO_IMAGE = 1
    private val RESULT_PHYSICAL_IMAGE = 2
    var mActivity: Activity? = null
    lateinit var apiSingleton: ApiSingletonClass
    var file: File? = null
    var picturePath: String? = null
    var picturePath1: String? = null
    var selectedImage: Uri? = null
    var selectedImage1: Uri? = null

    companion object {
        var homefragment: DashboardActivity? = null
        fun getInstance(): DashboardActivity {
            homefragment = DashboardActivity()

            return homefragment!!
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mActivity = activity
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as Activity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view: View = inflater.inflate(R.layout.activity_detail_actiivty, container, false)
        var scrollview = view.findViewById(R.id.scrollview) as ScrollView
        var logo_btn = view.findViewById(R.id.buttonLoadPicture) as Button
        var physical_btn = view.findViewById(R.id.buttonLoadPicture1) as Button
        var btn_card_new = view.findViewById(R.id.btn_card_new) as Button
        apiSingleton = ApiSingletonClass()
        logo_btn.setOnClickListener {
            val i = Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
            startActivityForResult(i, RESULT_LOGO_IMAGE)
        }

        physical_btn.setOnClickListener {
            val i = Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )

            startActivityForResult(i, RESULT_PHYSICAL_IMAGE)
        }

        btn_card_new.setOnClickListener {
            uploadFileEdit()
//            var intent = Intent(context, DrawerActivity::class.java)
//            intent.putExtra("redirect", "3")
//            context!!.startActivity(intent)
        }
        if (scrollview.visibility == View.GONE) {
            scrollview.visibility = View.VISIBLE
            val params = scrollview.getLayoutParams() as ConstraintLayout.LayoutParams
            val slide_up = AnimationUtils.loadAnimation(
                context,
                R.anim.slide_up
            )
            scrollview.setLayoutParams(params)
            scrollview.startAnimation(slide_up)
        }
        return view
    }

    fun uploadFileEdit() {

        val apiInterface = apiSingleton.getInstance().createService()
        var token =
            "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjI5ZWFiMDllNzQ4ZjdhYTRjYjNjNTM5ZWJkYzEyNWYzNjdlZTYwNTJmOTZlYjQ3ZTY3YzVlY2M3ODUzMDc0MGE2Y2QwNjE0OTE2MTUzMTEzIn0.eyJhdWQiOiIxIiwianRpIjoiMjllYWIwOWU3NDhmN2FhNGNiM2M1MzllYmRjMTI1ZjM2N2VlNjA1MmY5NmViNDdlNjdjNWVjYzc4NTMwNzQwYTZjZDA2MTQ5MTYxNTMxMTMiLCJpYXQiOjE1NzM0NzMxNTUsIm5iZiI6MTU3MzQ3MzE1NSwiZXhwIjoxNjA1MDk1NTU1LCJzdWIiOiI2Iiwic2NvcGVzIjpbXX0.jF0caYb6Vu-Y3nP8_qyb7-t_dIz9QsvOMvohjfQ4nVxOS3skIwm4ov0fwtmjrDbDmqqpAv9CRvBuuV8s3XJqdcARJyKgFq8Y1ks92DswYG1ktr7YmXCQwXHovFmCUJHq7zqRpj0P4HwkwMztNJnoGol6ObEJIoWSjPfuzBi-ai7QT4VCXZjtFsCdfFJ4xRwXaC9g2ay8AwsvJ9GrYo8ganTH65pksG_PhKPzanhL6QpyvbntB-oGrpPzPdbItRBapb5FJnxL6SFW8XL2aQFk5CkEJHyc-JmtLMdFhPw_U3V_JEq-rPJrw_rcv3ma5jk7wPDvLdeD8Cr8kxHyncMcWD_QnGiY_VImtrHNlGAjcUl1GlQaoDE46XVLiaWR7oggMI6_Tio6tjMsMXkMdTv6ffMZWNKiYCZR0MvUy6Pv2QwnDYlknG8v5KP2GtdcO7yt0iTq3FSSeoLUwTbJm2rpw5OjIcL1d5mYzcWMgnOee_YFLcd9EnCyDLCqcJs53R8cx4c1YV3kq17IV4ZeJr-X5XO-63xFhO55apOhd8jo51H-rSOw7KKmX7YpllgjRx_QE8i6nW32cZ_24q0yPbxMVgadQtHIqCgIL_ZDodm2ojo8AXPsr5j1DbfFXtJZuEbWQmdEuXtpHjHN5lq3dz5zhYQywIMVIXUPv5u7ueBgpgc"
        val map: HashMap<String, String> = HashMap()
        map["Accept"] = "application/json"
        map["Authorization"] = "Bearer $token"
        map["Content-Type"] = "multipart/form-data"

        val file = File(picturePath)
        val file1 = File(picturePath1)


        var fileBody = RequestBody.create(MediaType.parse(context!!.contentResolver.getType(selectedImage)), file)
        var fileBody1 = RequestBody.create(MediaType.parse(context!!.contentResolver.getType(selectedImage1)), file)

        var part = MultipartBody.Part.createFormData("logo_image", file.name, fileBody)
        var part1 = MultipartBody.Part.createFormData("physical_card_image", file1.name, fileBody1)


        val title = RequestBody.create(MultipartBody.FORM, "Techathalon")
        val designation = RequestBody.create(MultipartBody.FORM, "Android Developer")
        val phoneno = RequestBody.create(MultipartBody.FORM, "8879053038")
        val color1 = RequestBody.create(MultipartBody.FORM, "Blue")
        val altphoneno = RequestBody.create(MultipartBody.FORM, "90000584")
        val color2 = RequestBody.create(MultipartBody.FORM, "White")
        val color3 = RequestBody.create(MultipartBody.FORM, "Gray")
        val color4 = RequestBody.create(MultipartBody.FORM, "Pink")


        val call = apiInterface.getCardUrl(
            "application/json",
            "Bearer $token",
            title,
            designation,
            phoneno,
            altphoneno,
            color1,
            color2,
            color3,
            color4,
            part, part1
        )



        apiSingleton.getInstance().getData(call, object : RetrofitResponseInterface {
            override fun onServerFailure(obj: JSONObject) {
                Log.d("TAG", "RESPONSE FAILURE = " + obj.getString("message"))
//                    responseFailure(obj.getString("message"))
            }

            override fun <T> onSuccess(apiType: String, response: T) {
                Log.d("TAG", "RESPONSE SUCCESS = " + response)
                val userData = response as CardModel
                var i = Intent(context, DrawerActivity::class.java)
                i.putExtra("redirect","4")
                i.putExtra("card_url", userData.card_url!!.toString())
                context!!.startActivity(i)


            }
//                apiSingleton.dismissDialog(progressBar)
//                    val userData = response as RegisterModel
//                    val hashMap: HashMap<String, String> = HashMap()
//                    hashMap["token"] = userData.success!!.token.toString()
//                    sharedPref.setUserData(hashMap)
////                sharedPref.saveUserData(getString(R.string.user), Boolean::class.java, true)
//                    startActivity(Intent(applicationContext, LoginActivity::class.java))
//                    finish()


            override fun onFailure(apiType: String) {
//                    responseFailure("")
                Log.d("TAG", "RESPONSE FAIL = " + apiType)
            }


        })
    }

    fun getPath(uri: Uri) {
        val mimeType = getMimeType(uri)
        if (mimeType != null && mimeType != "") {
            if (mimeType.contains("image")) {
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(context!!.getContentResolver(), uri)
                    val sdCard = Environment.getExternalStorageDirectory()
                    val directory = File(sdCard.getAbsolutePath() + "/Visiting Card")
                    directory.mkdirs()
                    val newappendname = SimpleDateFormat("yyyyMMddhhmmss", Locale.getDefault()).format(Date())
                    val newimagename = "userimage_" + "_" + newappendname + ".png"
                    val fo: OutputStream? = null
                    val mFile2 = File(directory, newimagename)
                    val outStream: FileOutputStream
                    outStream = FileOutputStream(mFile2)
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream)
                    outStream.flush()
                    outStream.close()

                    picturePath = directory.getAbsolutePath() + "/" + newimagename
                    if (picturePath != "") {

                        file = File(picturePath)
                    }
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            } else {
                Toast.makeText(context, "Please select image file", Toast.LENGTH_LONG).show()
            }
        }
    }

    fun getPath1(uri: Uri) {
        val mimeType = getMimeType(uri)
        if (mimeType != null && mimeType != "") {
            if (mimeType.contains("image")) {
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(context!!.getContentResolver(), uri)
                    val sdCard = Environment.getExternalStorageDirectory()
                    val directory = File(sdCard.getAbsolutePath() + "/Visiting Card")
                    directory.mkdirs()
                    val newappendname = SimpleDateFormat("yyyyMMddhhmmss", Locale.getDefault()).format(Date())
                    val newimagename = "userimage_" + "_card" + newappendname + ".jpg"
                    val fo: OutputStream? = null
                    val mFile2 = File(directory, newimagename)
                    val outStream: FileOutputStream
                    outStream = FileOutputStream(mFile2)
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream)
                    outStream.flush()
                    outStream.close()

                    picturePath1 = directory.getAbsolutePath() + "/" + newimagename
                    if (picturePath1 != "") {

                        file = File(picturePath1)
                    }
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            } else {
                Toast.makeText(context, "Please select image file", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun getMimeType(uri: Uri): String? {
        var mimeType: String? = null
        if (uri.getScheme() == ContentResolver.SCHEME_CONTENT) {
            val cr = context!!.getContentResolver()
            mimeType = cr.getType(uri)
        } else {
            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri.toString())
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase())
        }
        return mimeType
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RESULT_LOGO_IMAGE && resultCode == Activity.RESULT_OK && null != data) {
            selectedImage = data.data

            getPath(selectedImage!!)
            val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

            val cursor = context!!.contentResolver.query(
                selectedImage!!,
                filePathColumn, null, null, null
            )
            cursor!!.moveToFirst()

            val columnIndex = cursor.getColumnIndex(filePathColumn[0])
            val picturePath = cursor.getString(columnIndex)
            cursor.close()

            val imageView = view!!.findViewById(R.id.img_logo) as ImageView
            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath))

        } else if (requestCode == RESULT_PHYSICAL_IMAGE && resultCode == Activity.RESULT_OK && null != data) {
            selectedImage1 = data.data

            getPath1(selectedImage1!!)
            val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

            val cursor = context!!.contentResolver.query(
                selectedImage!!,
                filePathColumn, null, null, null
            )
            cursor!!.moveToFirst()

            val columnIndex = cursor.getColumnIndex(filePathColumn[0])
            val picturePath = cursor.getString(columnIndex)
            cursor.close()

            val imageView = view!!.findViewById(R.id.img_logo1) as ImageView
            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath))

        }


    }
}
