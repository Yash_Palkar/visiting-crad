package com.client.visitingcard

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.*
import com.client.visitingcard.ApiManager.ApiSingletonClass
import com.client.visitingcard.HelperClass.CheckInternetConnection
import com.client.visitingcard.HelperClass.SharedPreferenceClass
import com.client.visitingcard.Interface.RetrofitResponseInterface
import com.client.visitingcard.Model.LoginModel
import org.json.JSONObject

class LoginActivity : AppCompatActivity(),View.OnClickListener {
    lateinit var edit_username: EditText
    lateinit var edit_password: EditText
    lateinit var btn_login: Button
    private lateinit var progressBar: ProgressBar

    lateinit var sharedPref: SharedPreferenceClass
    lateinit var apiSingleton: ApiSingletonClass
    private var checkInternet = CheckInternetConnection()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        sharedPref = SharedPreferenceClass(applicationContext)
        apiSingleton = ApiSingletonClass()
        checkInternet = CheckInternetConnection()

        edit_username = findViewById(R.id.edit_username)
        edit_password = findViewById(R.id.edit_password)
        btn_login = findViewById(R.id.btn_login)


        btn_login.setOnClickListener(this)
    }

    private fun userLogin() {
        val userEmail: String = edit_username.text.toString()
        val userPassword: String = edit_password.text.toString()
        if (userEmail == "" || userPassword == "") {
            Toast.makeText(applicationContext, "Enter Values First!", Toast.LENGTH_SHORT).show()
        } else {
            if (checkInternet.isNetworkAvailable(applicationContext)) {
//                apiSingleton.showDialog(progressBar)
                postLogin(userEmail, userPassword)
            } else {
                Toast.makeText(applicationContext, "Check Network Connectivity", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun postLogin(userEmail: String, password: String) {
        val apiInterface = apiSingleton.getInstance().createService()

        val map: HashMap<String, String> = HashMap()
        map["Accept"] = "application/json"
        map["Content-Type"] = "application/x-www-form-urlencoded"


        val call = apiInterface.getLoginService("application/json","application/x-www-form-urlencoded", userEmail, password)
        apiSingleton.getInstance().getData(call, object : RetrofitResponseInterface {
            override fun onServerFailure(obj: JSONObject) {
                Log.d("TAG", "RESPONSE FAILURE = " + obj.getString("message"))
                responseFailure(obj.getString("message"))
            }

            override fun <T> onSuccess(apiType: String, response: T) {
//                apiSingleton.dismissDialog(progressBar)
                val userData = response as LoginModel
                val hashMap: HashMap<String, String> = HashMap()
                hashMap["token"] = userData.success!!.token.toString()
                sharedPref.setUserData(hashMap)
                sharedPref.saveUserData(getString(R.string.user_login_status), Boolean::class.java, true)
                startActivity(Intent(applicationContext, DrawerActivity::class.java))
                finish()
            }

            override fun onFailure(apiType: String) {
                responseFailure("")
            }
        })
    }

    fun responseFailure(message : String) {
//        apiSingleton.dismissDialog(progressBar)


    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btn_login -> {
                userLogin()
//                startActivity(Intent(applicationContext, DrawerActivity::class.java))
//                finish()
            }
        }
    }
}