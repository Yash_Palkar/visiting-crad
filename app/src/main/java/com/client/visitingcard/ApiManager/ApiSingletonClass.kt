package com.client.visitingcard.ApiManager

import android.util.Log
import android.view.View
import android.widget.ProgressBar
import com.client.visitingcard.Interface.RetrofitInterfaceClass
import com.client.visitingcard.Interface.RetrofitResponseInterface
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import java.io.IOException


class ApiSingletonClass {
    private var instance: ApiSingletonClass? = null
    private var retrofit: Retrofit? = null
    private val connectionTimeoutMin: Long = 60
    private val writeTimeoutMin: Long = 60
    private val readTimeoutMin: Long = 60

    @Synchronized
    private fun createInstance() {
        if (instance == null) {
            instance = ApiSingletonClass()
        }
    }

    fun getInstance(): ApiSingletonClass {
        if (instance == null) {
            createInstance()
        }
        return instance!!
    }

    fun <T> getData(call: Call<T>, apiListener: RetrofitResponseInterface) {
        call.enqueue(object : Callback<T> {
            override fun onFailure(call: Call<T>, t: Throwable) {
                apiListener.onFailure("FILTER")
                Log.d("TAG", "RESPONSE FAILURE")
            }

            override fun onResponse(call: Call<T>, response: Response<T>) {
                if (response.isSuccessful) {
                    try {
                        apiListener.onSuccess("FILTER", response.body()!!)
                        Log.d("TAG", "RESPONSE = $response")
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Log.d("TAG", "Exception = $e")
                    }
                } else {
                    try {
                        val jObjError = JSONObject(response.errorBody()!!.string())
                        apiListener.onServerFailure(jObjError)
                    } catch (e: Exception) {
                        Log.d("TAG", "EXCEPTION = " + e.message)
                    }
                }
            }
        })
    }


    private fun getRetrofit(): Retrofit {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val oktHttpClient = OkHttpClient.Builder()
        oktHttpClient.addInterceptor(object : Interceptor {
            @Throws(IOException::class)
            override fun intercept(chain: Interceptor.Chain): okhttp3.Response? {
                val original = chain.request()
             var token="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjI5ZWFiMDllNzQ4ZjdhYTRjYjNjNTM5ZWJkYzEyNWYzNjdlZTYwNTJmOTZlYjQ3ZTY3YzVlY2M3ODUzMDc0MGE2Y2QwNjE0OTE2MTUzMTEzIn0.eyJhdWQiOiIxIiwianRpIjoiMjllYWIwOWU3NDhmN2FhNGNiM2M1MzllYmRjMTI1ZjM2N2VlNjA1MmY5NmViNDdlNjdjNWVjYzc4NTMwNzQwYTZjZDA2MTQ5MTYxNTMxMTMiLCJpYXQiOjE1NzM0NzMxNTUsIm5iZiI6MTU3MzQ3MzE1NSwiZXhwIjoxNjA1MDk1NTU1LCJzdWIiOiI2Iiwic2NvcGVzIjpbXX0.jF0caYb6Vu-Y3nP8_qyb7-t_dIz9QsvOMvohjfQ4nVxOS3skIwm4ov0fwtmjrDbDmqqpAv9CRvBuuV8s3XJqdcARJyKgFq8Y1ks92DswYG1ktr7YmXCQwXHovFmCUJHq7zqRpj0P4HwkwMztNJnoGol6ObEJIoWSjPfuzBi-ai7QT4VCXZjtFsCdfFJ4xRwXaC9g2ay8AwsvJ9GrYo8ganTH65pksG_PhKPzanhL6QpyvbntB-oGrpPzPdbItRBapb5FJnxL6SFW8XL2aQFk5CkEJHyc-JmtLMdFhPw_U3V_JEq-rPJrw_rcv3ma5jk7wPDvLdeD8Cr8kxHyncMcWD_QnGiY_VImtrHNlGAjcUl1GlQaoDE46XVLiaWR7oggMI6_Tio6tjMsMXkMdTv6ffMZWNKiYCZR0MvUy6Pv2QwnDYlknG8v5KP2GtdcO7yt0iTq3FSSeoLUwTbJm2rpw5OjIcL1d5mYzcWMgnOee_YFLcd9EnCyDLCqcJs53R8cx4c1YV3kq17IV4ZeJr-X5XO-63xFhO55apOhd8jo51H-rSOw7KKmX7YpllgjRx_QE8i6nW32cZ_24q0yPbxMVgadQtHIqCgIL_ZDodm2ojo8AXPsr5j1DbfFXtJZuEbWQmdEuXtpHjHN5lq3dz5zhYQywIMVIXUPv5u7ueBgpgc"

                val request = original.newBuilder()
                    .header("Authorization", "Bearer $token")
                    .method(original.method(), original.body())
                    .build()


                val response = chain.proceed(request)
                Log.d("MyApp", "Code : " + response.code())
                return if (response.code() == 401) {
                    // Magic is here ( Handle the error as your way )
                    response
                } else response
            }
        })
//            .connectTimeout(connectionTimeoutMin, TimeUnit.MINUTES)
//            .writeTimeout(writeTimeoutMin, TimeUnit.MINUTES)
//            .readTimeout(readTimeoutMin, TimeUnit.MINUTES)

        retrofit = Retrofit.Builder()
            .baseUrl("http://192.168.0.109")//TODO base url insert
            .addConverterFactory(GsonConverterFactory.create())
            .client(oktHttpClient.build())
            .build()
        return retrofit!!
    }

    fun createService(): RetrofitInterfaceClass {
        return getRetrofit().create(RetrofitInterfaceClass::class.java)
    }

    fun showDialog(progressBar: ProgressBar) {
        if (progressBar != null)
            progressBar.visibility = View.VISIBLE
    }

    fun dismissDialog(progressBar: ProgressBar) {
        if (progressBar != null)
            progressBar.visibility = View.GONE
    }

}